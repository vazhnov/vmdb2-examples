# vmdb2 examples

`vmdb2` is a Python tool, which can create bootable images of Debian-based distros.

In addition to `debootstrap`, also configures partitioning and a boot loader, supports EFI, cryptsetup/LUKS and Ansible.

Also, instead of image, `vmdb2` can install GNU/Linux to HDD/SSD/NVME/USB flash drives.

To create an image, `vmdb2` requires a YAML specification file with _steps_ (in the documentation, they called .vmdb). The official repository has only a few examples of such files.

This repository has more examples for `vmdb2`.

## Usage example

```sh
sudo mount -o remount,size=12G /tmp
cd /tmp
git clone 'https://gitlab.com/vazhnov/vmdb2-examples.git'
VM_NAME="Devuan_6_excalibur_minbase"
sudo vmdb2 "./vmdb2-examples/Examples/${VM_NAME}_with_caching.yaml" --output "/tmp/vmdb2_${VM_NAME}.img" --rootfs-tarball "/tmp/vmdb2_${VM_NAME}_rootfs_cache.tar.gz" -v --log "/tmp/vmdb2_${VM_NAME}.log"
sudo chown -v "$USER" "/tmp/vmdb2_$VM_NAME.img"
qemu-system-x86_64 -enable-kvm -m 2048 -net nic -net user -drive format=raw,if=virtio,file="/tmp/vmdb2_${VM_NAME}.img"
```

## Links

* https://vmdb2.liw.fi — the official web-page.
* https://vmdb2-manual.liw.fi — the manual.
* https://gitlab.com/larswirzenius/vmdb2 — sources.
* https://app.radicle.xyz/nodes/radicle.liw.fi/rad:z2kxCtBwDQMPcaf9vGTNH5nYkp9qk/tree/main — sources at peer-to-peer code collaboration stack.
* https://tracker.debian.org/pkg/vmdb2

### Projects which uses `vmdb2`

* https://salsa.debian.org/raspi-team/image-specs/ — Raspberry Pi images are build by `vmdb2`.
* https://github.com/Jerome-Maurin/vmdb2-wrapper — a lot of examples for SBCs, + a fork https://github.com/jean-marc-LACROIX/vmdb2-wrapper
* https://github.com/optimeas/vmdb2-image-smartmini-tx6/ — for ARM imx.6 computers?
* https://github.com/Torxgewinde/BrowserBox (Bash, GPL v3) — creates an image with Firefox ESR.

### Another tools

* https://wiki.debian.org/SystemBuildTools — review of different tools.
* https://gitlab.com/vazhnov/devuan-debootstrap-grub-efi — setup Devuan to HDD/SSD/USB/SD card.
